import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_docker_containers(host):
    with host.sudo():
        assert host.docker("swagger_ui").is_running
        assert host.docker("traefik_proxy").is_running


def test_swagger_api_url(host):
    cmd = host.run("curl --insecure --fail https://ics-ans-role-swagger-ui-default/")
    assert cmd.rc == 0
    assert 'url: "http://petstore.swagger.io/v2/swagger.json"' in cmd.stdout

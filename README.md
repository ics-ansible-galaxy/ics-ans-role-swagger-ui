ics-ans-role-swagger-ui
===================

Ansible role to install Swagger-UI.

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

Role Variables
--------------

```yaml
swaggerui_port: 8080
swaggerui_image: swaggerapi/swagger-ui:3.16.0
swaggerui_network: swagger-network
swaggerui_frontend_rule: "Host:{{ ansible_fqdn }}"
swaggerui_container_name: swagger_ui
swaggerui_base_url:
swaggerui_api_url:
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-swagger-ui
```

License
-------

BSD 2-clause
